<?require_once 'PHPExcel/PHPExcel.php';
require_once 'PHPExcel/PHPExcel/Writer/Excel5.php';
require_once 'PHPExcel/PHPExcel/IOFactory.php';
$link = mysqli_connect("127.0.0.1", "root", "root", "users"); 
 if (!$link) {
    echo "Невозможно подключиться к серверу." ;
}
$array = array("№ п/п", "Название", "Жанр", "Разработчик", "Издатель", "Цифровой ключ",  "Дата приобретения", "Дата окончания", "url магазина");
$xls = new PHPExcel();
$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet();
$sheet->setTitle('Игры');

$j=3;
$query = "SELECT * FROM rez";
$result = mysqli_query($link, $query) or die("Не могу выполнить запрос!");
while ($row=mysqli_fetch_array($result)){
    for($i = 0; $i < count($row); $i++){
        $text = $row[$i];
        $sheet->setCellValueByColumnAndRow($i, $j, $text);       
    }
$j++;
}
$sheet->setCellValueExplicit('A1', 'Игры', PHPExcel_Cell_DataType::TYPE_STRING);
$sheet->mergeCells('A1:I1');
$sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

for($i = 0; $i < count($array); $i++){
    $sheet->setCellValueByColumnAndRow($i, 2, $array[$i]);
    $sheet->getStyleByColumnAndRow($i, 2)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
}
header("Content-Disposition: attachment; filename=game.xls");
$objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
$objWriter->save('php://output');
?>